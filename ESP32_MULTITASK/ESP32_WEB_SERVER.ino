
#include <WiFi.h>
#include <WebServer.h>

// Plus d'informations sur https://lastminuteengineers.com/creating-esp32-web-server-arduino-ide/
/*Put your SSID & Password*/
const char *ssid = "VG_EXTERNAL";
const char *password = "wlan-4-vaillant";

WebServer server(80);

String customText = "CUSTOM";

void setup() {
  Serial.begin(115200);
  delay(100);

  Serial.println("Connecting to ");
  Serial.println(ssid);

  //connect to your local wi-fi network
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
  delay(1000);
  Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");  Serial.println(WiFi.localIP());

  server.on("/", handle_OnConnect);
  server.onNotFound(handle_NotFound);

  server.begin();
  Serial.println("HTTP server started");
}
void loop() {
  server.handleClient();
}

void handle_OnConnect() {
  server.send(200, "text/html", SendHTML(customText)); 
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

String SendHTML(String customText){
  String ptr = "<!DOCTYPE html> <html>\n";
 ptr+="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr+="<title>RSS feed control</title>\n";
  ptr+="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr+="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}\n";
  ptr+=".button {display: block;width: 80px;background-color: #3498db;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  ptr+=".button-on {background-color: #3498db;}\n";
  ptr+=".button-on:active {background-color: #2980b9;}\n";
  ptr+=".button-off {background-color: #34495e;}\n";
  ptr+=".button-off:active {background-color: #2c3e50;}\n";
  ptr+=" {font-size: 14px;color: #888;margin-bottom: 10px;}\n";
  ptr+="</style>\n";
  ptr+="</head>\n";
  ptr+="<body>\n";
  ptr+="<h1>ESP32 Web Server</h1>\n";
  ptr+="<h3>Do you really want to change these values? ...</h3>\n";
  ptr+="\n\n";
  ptr+="<input type=\"checkbox\" onclick=\"var input = document.getElementById('inputText'); var dropdown = document.getElementById('rss-select'); if(this.checked){ input.disabled = false; input.focus();dropdown.disabled = true;}else{input.disabled=true;dropdown.focus();dropdown.disabled = false;}\">Custom text input  </input>\n";
  ptr+=" <input id=\"inputText\" name=\"Custom Text\" disabled=\"disabled\"/ value=\"";
  ptr+=customText;
  ptr+="\">\n";
  ptr+="<br><br>";
  ptr+="Else <select id=\"rss-select\">";
    ptr+="<option value=\"\">--Please choose an option--</option>";
    ptr+="<option value=\"poule-qui-mue\">Poule qui mue</option>";
    ptr+="<option value=\"leGorafi\">LeGorafi</option>";
  ptr+="</select>";
  ptr+="</body>\n";
  ptr+="</html>\n";
  return ptr;
}
