# Magical_ESP32

### A creative project based on ESP 32 and an RGB LED matrix

This project is part of a the Maker Project 2019.<br/>
The goal is to realize a project using one (or more) [ESP32](https://www.espressif.com/en/products/hardware/esp32/overview) with one (or more) [16x32 Adafruit LED matrix](https://www.adafruit.com/product/420).


### List of ideas

| Project Name  | Description  |
|---|---|
| "Tapa-Toy" |  The project is a spelling device for KIDS. The goal is to have NFC tags that will be programmed with name ( and eventually the pronounciation) of the toy. This tag will be attached to the toy. When your kid put the toy close to our reader, the name of the toy will appear on the LED Matrix ( and eventually the name will be pronounced ). There will be two device communicating with each other. One for reading NFC data from the Tag attached to the toy, the other will be in charge of receiving the name and display the name on screen. The goal is to allow to your kid to lear the name of oobject and especially there pronounciation. It will be helpful to help kids to associate name of animals, vegetables or fruit easily and for those who are learning to read/write, to know how to write the name of their favorite animal or vegetables.  |
|||
 
### Tutorial & Ressources

Link for ressources  : [ESP32 with Adafruit](https://www.ebenfeld.de/2019/05/16/esp32-with-adafruit-medium-rgb-matrix-panel/)

#### IDE proposal

Eclipse using ESP-IDF : [Eclipse with ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/eclipse-setup.html)<br/>
Plugin for Visual : [Arduino plugin for Visual](https://www.visualmicro.com/page/Arduino-Visual-Studio-Downloads.aspx)
jetbrains CLion : https://www.jetbrains.com/clion/