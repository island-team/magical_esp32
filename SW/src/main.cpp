#include <Arduino.h>
#include <WiFi.h>
#include <SPI.h>
#include <RGBmatrixPanel.h>
#include <HTTPClient.h>

#if 0

#define CLK  4   // USE THIS ON ARDUINO UNO, ADAFRUIT METRO M0, etc.
//#define CLK A4 // USE THIS ON METRO M4 (not M0)
//#define CLK 11 // USE THIS ON ARDUINO MEGA
#define OE   0
#define LAT 2
#define A   12
#define B   13
#define C   14
// Last parameter = 'true' enables double-buffering, for flicker-free,
// buttery smooth animation.  Note that NOTHING WILL SHOW ON THE DISPLAY
// until the first call to swapBuffers().  This is normal.
const uint8_t rgbpins[] = { 17, 25, 33, 32, 35, 34 };
#endif


// °°°°°°°°°°°°°°°° Display °°°°°°°°°°°°°°°°
#define CLK  4
#define OE   0
#define LAT 2
#define A   12
#define B   13
#define C   14

#define FPS 55

const uint8_t rgbpins[] = { 17, 18, 19, 21, 22, 23 };
RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, true, (uint8_t *)rgbpins, 3);
static uint32_t prevTime = 0;
int16_t textX = matrix.width();
int16_t textMin = 0;
String currentTextToDisplay ="Waiting init...";
const int colors[5] = {55,356,678,897,1250};


// °°°°°°°°°°°°°°°° RSS data °°°°°°°°°°°°°°°°
#define NB_FLOW_BY_SITE 8
#define NB_OF_RSSB_FEED 5
String data[NB_FLOW_BY_SITE];

const String listOfFeed[NB_OF_RSSB_FEED] = { "http://feeds.reuters.com/reuters/scienceNews",
                                             "http://feeds.reuters.com/reuters/sportsNews",
                                             "http://feeds.reuters.com/reuters/technologyNews",
                                             "http://feeds.reuters.com/reuters/topNews",
                                             "http://feeds.reuters.com/reuters/entertainment"};
                           
/*const String listOfFeed[5] = {"https://lapoulequimue.fr/feed/",
                              "https://www.francetvinfo.fr/titres.rss",
                              "https://www6.lequipe.fr/rss/actu_rss_NBA.xml",
                              "https://www.presseocean.fr/taxonomy/term/20985/0/feed"  };
*/

// °°°°°°°°°°°°°°°° Network connexion °°°°°°°°°°°°°°°°
const char *ssid = "VG_EXTERNAL";
const char *password = "wlan-4-vaillant";


// °°°°°°°°°°°°°°°° Parsing characters °°°°°°°°°°°°°°°°
const struct TStringChange
{
  String stringToFind;
  String stringReplacement;
}stringChange[] =
{
  {"&#8217;","'"},
  {"&#8211;","–"},
  {"&amp;nbsp;", " "},
  {"é","e"},
  {"è","e"},
  {"ê","e"},
  {"ë","e"},
  {"ç","c"},
  {"à","a"},
  {"ù","u"},
  {"û","u"},
  {"ô","o"},
  {"ö","o"},
  {"ï","i"},
  {"î","i"},
  };


int extractItem(String feed, String * tab)
{
  int startpos = feed.indexOf("<item>");
  int endpos = feed.indexOf("</item>");
  if (startpos!= -1 && endpos!= -1)
  {
    *tab = feed.substring((startpos + 6), endpos) ;
    return (endpos + 6 );
  }
  else
  {
    return - 1;
  }
  
}

int extractTitle(String feed, String * tab)
{
  int startpos =feed.indexOf("<title>");
  int endpos = feed.indexOf("</title>");
  if (startpos!= -1 && endpos!= -1)
  {
     *tab = feed.substring((startpos + 7),endpos);
     return (endpos + 7 );
  }
  else
  {
    return -1;
  }
}

int getflow(String feed, String * tab )
{
  int posi;
  posi = extractItem(feed, tab);
  if (posi != -1)
  {
    if (extractTitle(feed, tab) == -1)
    {
      *tab = "";
      posi = -1;
    }
  }  
  return posi;
}


// °°°°°°°°°°°°°°°° Main °°°°°°°°°°°°°°°°

void setup() 
{
  matrix.begin();
  matrix.setTextWrap(false); // Allow text to run off right edge
  matrix.setTextSize(2);
  
  // Clear background
  matrix.fillScreen(0); 
 // Draw big scrolly text on top


  Serial.begin(115200);
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid,password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() 
{
    static bool getfeed = true;
    static int index = 0;
    static int indexText = 0;
    static int indexFeed = 0;
    static String feed ="";

    HTTPClient client;
  
    
   
    if (getfeed)
    {
      if (client.begin(listOfFeed[indexFeed]))
      {
        int httpCode = client.GET();
        if(httpCode > 0)
        {
          if(httpCode == HTTP_CODE_OK) 
          {
              feed = client.getString();
            //  Serial.println(feed);
            if (feed.length() > 0)
            {
              for (int j = 0; j < sizeof(stringChange) / sizeof(struct TStringChange); j++) 
              {
                int index = 0;
                while (index < feed.length())
                {
                  index = feed.indexOf(stringChange[j].stringToFind);
                  if (index != -1)
                  {
                    feed.replace(stringChange[j].stringToFind,stringChange[j].stringReplacement);
                  }
                }
              }
              getfeed = false;
              if (indexFeed >= NB_OF_RSSB_FEED)
              {
                indexFeed = 0;
              }
              else
              {
                indexFeed++;
              }
              
            }
          }
        }
      }
      else 
      {
         if (indexFeed >= 5)
              {
                indexFeed = 0;
              }
              else
              {
                indexFeed++;
              }
      }
    }

    if (index< NB_FLOW_BY_SITE)
    {
      if (feed!="")
      {
        int posi = getflow(feed, &data[index]);
        if (posi != -1) feed = feed.substring(posi, feed.length()); 
        Serial.println(data[index]);
      }
    }

    index++;

    matrix.fillScreen(0);
    matrix.setTextColor(matrix.ColorHSV(colors[indexFeed], 255, 255, true));
    matrix.setCursor(textX, 1 );
    matrix.print(currentTextToDisplay);
    textMin = currentTextToDisplay.length() * -12;
    if ((--textX) < textMin) 
    {
      textX = matrix.width();
      currentTextToDisplay = data[indexText];
      indexText++;
      if (indexText>=NB_FLOW_BY_SITE)
      {
        indexText = 0;
        index = 0;
        feed="";
        getfeed = true;
      }
    }
    uint32_t t;
    while (((t = millis()) - prevTime) < (1000 / FPS));
    prevTime = t;
    matrix.swapBuffers(false);


}